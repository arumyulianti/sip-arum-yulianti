<?php

use Illuminate\Database\Seeder;

class rolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
        	'name' => Str::random(10).'Arum Yulianti' ,
        ]);

        $this->call(rolesTableSeeder::class);
    }
}
