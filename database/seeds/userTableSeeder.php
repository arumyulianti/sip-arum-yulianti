<?php

use Illuminate\Database\Seeder;
/*use Illuminate\Support\Facedes\DB;
use Illuminate\Support\Facedes\Hash;
use Illuminate\Support\Str;*/

class userTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user')->insert([
        	'name' => Str::random(20).'Arum Yulianti' ,
        	'username' => Str::random(20).'arum' ,
        	'email' => Str::random(20).'@arum.com' ,
        	'password' => Hash::make('password')
        ]);

        $this->call(userTableSeeder::class);
    }
}
